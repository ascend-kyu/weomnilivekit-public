# WeOmniLiveKit-Public

[![CI Status](https://img.shields.io/travis/Chayanon Ardkham/WeOmniLiveKit-Public.svg?style=flat)](https://travis-ci.org/Chayanon Ardkham/WeOmniLiveKit-Public)
[![Version](https://img.shields.io/cocoapods/v/WeOmniLiveKit-Public.svg?style=flat)](https://cocoapods.org/pods/WeOmniLiveKit-Public)
[![License](https://img.shields.io/cocoapods/l/WeOmniLiveKit-Public.svg?style=flat)](https://cocoapods.org/pods/WeOmniLiveKit-Public)
[![Platform](https://img.shields.io/cocoapods/p/WeOmniLiveKit-Public.svg?style=flat)](https://cocoapods.org/pods/WeOmniLiveKit-Public)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WeOmniLiveKit-Public is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WeOmniLiveKit-Public'
```

## Author

Chayanon Ardkham, chayanon.ard@ascendcorp.com

## License

WeOmniLiveKit-Public is available under the MIT license. See the LICENSE file for more info.
